data<-read.table("F:/r_bioinfo/bigwork/GSE218095_counts.txt",header=T)
data<-data[,2:7]
library(DESeq2)
install.packages("writexl")
library(writexl)
WT_samples<-data[, grepl("WT", colnames(data))]
CKO_samples<-data[, grepl("CKO", colnames(data))]
colData<-data.frame(rownames=colnames(data),
                    group_list= factor(c(rep("WT", ncol(WT_samples)), rep("CKO", ncol(CKO_samples)))))
# 创建 DESeq2 的数据对象
dds<- DESeqDataSetFromMatrix(countData = data,
                             colData = colData,
                             design = ~ group_list)
# 使用 DESeq2 进行差异分析
dds<- DESeq(dds)
data<-read.table("F:/r_bioinfo/bigwork/GSE218095_counts.txt",header=T)
# 提取差异表达基因
results<- results(dds)
results<- as.data.frame(results)
results$id<-data$ID
write_xlsx(results,path ="F:/r_bioinfo/bigwork/GSE218095result.xlsx")


library(ggplot2)
library(ggrepel)
library(readxl)

results$Log10pvalue <- -log10(results$pvalue)
threshold_p <- 0.05
threshold_fc_up <- log2(2)
threshold_fc_down <- -log2(2)

results$Significance <- ifelse(results$pvalue< threshold_p & results$log2FoldChange > threshold_fc_up, "Upregulated",
                                     ifelse(results$pvalue< threshold_p & results$log2FoldChange < threshold_fc_down, "Downregulated", "Not significant"))
results$id <- data$ID
upregulated_genes <- results[results$Significance == "Upregulated", ]
upregulated_genes <- na.omit(upregulated_genes)
downregulated_genes <- results[results$Significance == "Downregulated", ]
downregulated_genes <- na.omit(downregulated_genes)
write_xlsx(upregulated_genes,path ="F:/r_bioinfo/bigwork/upregulated_genes.xlsx")
write_xlsx(downregulated_genes,path ="F:/r_bioinfo/bigwork/downregulated_genes.xlsx")
p <- ggplot(results, aes(x = log2FoldChange, y = Log10pvalue, color = Significance)) +
  geom_point(alpha=0.5) +
  scale_color_manual(values = c("Upregulated" = "red", "Downregulated" = "blue", "Not significant" = "grey")) +
  theme_minimal() +
  labs(title = "Volcano Plot", x = "Log2 Fold Change", y = "-Log10 P-value")
print(p)
ggsave("volcano_plot.jpg", plot = p, width = 10, height = 8, dpi = 300)



#KEGG通路富集
library(stringi)
library(ggplot2)
kegg<-read.table("F:/r_bioinfo/bigwork/kegg.txt", sep = "\t", header = TRUE)
enrich<-kegg
enrich_signif=enrich[which(enrich$PValue<0.05),]

head(enrich_signif)
enrich_signif=data.frame(enrich_signif)
KEGG=enrich_signif
KEGG$Term<-stri_sub(KEGG$Term,10,100)

ggplot(KEGG, aes(x = Count, y = Term)) +
  geom_point(aes(color = PValue, size = Count)) +
  scale_color_gradient(low = 'slateblue4', high = 'firebrick3') +
  theme_bw() +
  theme(panel.grid.minor = element_blank(), panel.grid.major = element_blank())


#GO通路富集
#CC
install.packages("stringi")
library(stringi)
GO_CC<-read.table("F:/r_bioinfo/bigwork/GO_CC.txt",sep="\t",header=T)
GO_CC_signif=GO_CC[which(GO_CC$PValue<0.05),]
head(GO_CC_signif)
GO_CC_signif$Term<-stri_sub(GO_CC_signif$Term,12,100)
#BP
GO_BP<-read.table("F:/r_bioinfo/bigwork/GO_BP.txt",sep="\t",header=T)
GO_BP_signif=GO_BP[which(GO_BP$PValue<0.05),]
head(GO_BP_signif)
GO_BP_signif$Term<-stri_sub(GO_BP_signif$Term,12,100)
#MF
GO_MF<-read.table("F:/r_bioinfo/bigwork/GO_MF.txt",sep="\t",header=T)
GO_MF_signif=GO_MF[which(GO_MF$PValue<0.05),]
head(GO_MF_signif)
library(dplyr)
enrich_signif=rbind(GO_BP_signif,rbind(GO_CC_signif,GO_MF_signif))#将三个数据框（DataFrame）GO_BP_signif、GO_CC_signif和GO_MF_signif按行合并（使用rbind函数），并将结果存储在名为enrich_signif的新数据框中。
go=enrich_signif
go=arrange(go,go$Category,go$PValue)#将enrich_signif数据框赋值给go变量，然后使用arrange函数对go数据框进行排序。排序首先按照Category列进行升序排序，然后在每个Category内部按照PValue列进行升序排序。
m=go$Category
m=gsub("TERM","",m)
m=gsub("_DIRECT","",m)
go$Category=m
GO_term_order=factor(as.integer(rownames(go)),labels = go$Term)
COLS<-c("#66C3A5","#8da1cb","#fd8d62")
ggplot(data=go,aes(x=GO_term_order,y=Count,fill=Category))+
  geom_bar(stat = "identity",width=0.8)+
  scale_fill_manual(values=COLS)+
  theme_bw()+
  xlab("Terms")+
  ylab("Gene_counts")+
  labs()+
  theme(axis.text.x = element_text(face="bold",color="black",angle=90,vjust=1,hjust=1))
write_xlsx(go,path ="F:/r_bioinfo/bigwork/goresult.xlsx")
